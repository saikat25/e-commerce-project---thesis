Django==1.8.12
MySQL-python==1.2.5
Pillow==2.9.0
django-flat-theme==1.1.0
httplib2==0.9.2
markdown2==2.3.1
pytz==2016.3
six==1.10.0
twilio==5.4.0
uWSGI==2.0.12
wsgiref==0.1.2
xlwt==1.0.0
