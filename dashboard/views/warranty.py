from django.shortcuts import render
from django.http.response import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from main.models import *
from main.utils import send_sms


@login_required
@user_passes_test(lambda u: u.is_staff)
def warranty_claims_view(request):
    warranty_claims = WarrantyClaim.objects.filter(warranty_status='claimed')
    return render(request, 'dashboard/warranty/warranty_claims.html', {'warranty_claims': warranty_claims})


@login_required
@user_passes_test(lambda u: u.is_staff)
def warranty_details_view(request, warranty_id):
    warranty_claim = WarrantyClaim.objects.get(pk=int(warranty_id))

    if request.method == 'POST':
        is_accpeted = request.POST.get('acceptClaim', 'no') == 'yes'
        msg = request.POST.get('smsReply')

        if is_accpeted:
            status = 'accepted'
            text = "Dear customer, your warranty claim (ID: {}) has been accepted.".format(warranty_claim.id)
        else:
            status = 'rejected'
            text = "Sorry, your warranty claim (ID: {}) has been rejected!".format(warranty_claim.id)

        if msg is not None:
            text += "\n\nAdmin says: {}".format(msg)
        else:
            if not is_accpeted:
                text += "\n\nFor more info, please contact support."

        warranty_claim.warranty_status = status
        warranty_claim.save()

        send_sms(warranty_claim.cartitem.cart.contact_phone, text)

        return HttpResponse('<script>window.close();</script>')

    return render(request, 'dashboard/warranty/warranty_claim_details.html', {'warranty_claim': warranty_claim})
