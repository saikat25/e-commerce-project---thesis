from dashboard.views.mainview import *
from dashboard.views.orders import *
from dashboard.views.warranty import *
from dashboard.views.category import *
from dashboard.views.products import *
from dashboard.views.branches import *
from dashboard.views.inventory import *

