from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from dashboard.utils import *
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Sum
from django.utils import timezone
from django.contrib import messages
from dashboard.utils import get_hottest_items
import json


@login_required
@user_passes_test(lambda u: u.is_staff)
def dashboard_home_view(request):
    start_datetime = timezone.now() - timedelta(days=7)
    end_datetime = timezone.now()
    hottest = get_hottest_items(start_datetime, end_datetime)
    weekly_sales = weekly_sale_counts()
    return render(request, 'dashboard/dashboard.html', {'hottest': json.dumps(hottest),
                                                        'weekly': json.dumps(weekly_sales),
                                                        'products': [p.as_dict() for p in Product.objects.all()]})


@login_required
@user_passes_test(lambda u: u.is_staff)
def period_sales_view(request):
    today = None in (request.GET.get('startDateTime'), request.GET.get('endDateTime'))

    start_datetime = request.GET.get('startDateTime', datetime.today().strftime('%Y-%m-%d 00:00:00'))
    end_datetime = request.GET.get('endDateTime', datetime.today().strftime('%Y-%m-%d 23:59:59'))

    sales_set = get_all_sales_in_range(start_datetime, end_datetime)
    pending_set = get_all_sales_in_range(start_datetime, end_datetime, is_complete=False)

    revenue_completed = sales_set.aggregate(total=Sum('amount_charged')).get('total') or 0
    revenue_pending = reduce(lambda x, y: x + y.actual_price(), pending_set, 0)

    revenue = revenue_completed + revenue_pending

    return render(request, 'dashboard/reports/period_sales.html',
                  {'today': today,
                   'revenue': revenue,
                   'total_sales': sales_set.count(),
                   'orders_date': sales_set,
                   'start_datetime': start_datetime,
                   'end_datetime': end_datetime})


@login_required
@user_passes_test(lambda user: user.is_staff)
def item_sales_view(request):
    today = None in (request.GET.get('startDateTime'), request.GET.get('endDateTime'))

    start_datetime = request.GET.get('startDateTime', datetime.today().strftime('%Y-%m-%d 00:00:00'))
    end_datetime = request.GET.get('endDateTime', datetime.today().strftime('%Y-%m-%d 23:59:59'))

    start_datetime_obj = tz_aware_datetime_from_string(start_datetime)
    end_datetime_obj = tz_aware_datetime_from_string(end_datetime)

    top_items = get_hottest_items(start_datetime_obj, end_datetime_obj, 5)
    all_items = get_all_items_by_number_of_sales(start_datetime_obj, end_datetime_obj)
    print len(all_items)

    return render(request, 'dashboard/reports/item_sales.html',
                  {'today': today,
                   'top_items': json.dumps(top_items),
                   'all_items': all_items,
                   'total_items': len(all_items),
                   'start_datetime': start_datetime,
                   'end_datetime': end_datetime})


@login_required
@user_passes_test(lambda user: user.is_staff)
def report_to_xls(request):
    start_datetime = request.GET.get('startDateTime', datetime.today().strftime('%Y-%m-%d 00:00:00'))
    end_datetime = request.GET.get('endDateTime', datetime.today().strftime('%Y-%m-%d 23:59:59'))

    sales_set = get_all_sales_in_range(tz_aware_datetime_from_string(start_datetime),
                                       tz_aware_datetime_from_string(end_datetime))
    xls = generate_xls_1(sales_set)
    file_name = 'Sales_Report_({:%d-%m-%Y-%H-%M}).xls'.format(datetime.today())
    response = HttpResponse(content_type="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename={}'.format(file_name)
    xls.save(response)
    return response


@login_required
@user_passes_test(lambda user: user.is_staff)
def user_history_view(request):
    try:
        user = User.objects.get(username=request.GET.get('username'))
        purchases = user.customer.cart_set.all()
        return render(request, 'dashboard/reports/user_history.html', {'purchases': purchases,
                                                                       'target_user': user})
    except User.DoesNotExist:
        messages.error(request, 'User does not exist.')
        return HttpResponseRedirect('/dashboard')



@login_required
@user_passes_test(lambda user: user.is_staff)
def product_history_view(request):
    try:
        product = Product.objects.get(pk=int(request.GET.get('product_id')))
        history = []
        for ci in CartItem.objects.filter(product=product, cart__completed_at__isnull=False):
            history.append({'checkout_at': '{:%B %d, %Y}'.format(ci.cart.checkout_at),
                            'customer_name': ci.cart.customer.user.get_full_name(),
                            'username': ci.cart.customer.user.username,
                            'quantity': ci.quantity})
        return render(request, 'dashboard/reports/product_history.html', {'product': product,
                                                                          'history': history})
    except Product.DoesNotExist:
        messages.error(request, 'Product does not exist.')
        return HttpResponseRedirect('/dashboard')

