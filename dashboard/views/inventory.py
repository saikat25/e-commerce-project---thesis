from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from main.models import *


@login_required
@user_passes_test(lambda u: u.is_staff)
def inventory_history_view(request):
    hist = StockTransfer.objects.all()
    products = Product.objects.all()
    return render(request, 'dashboard/inventory/inventory_history.html', {'transfers': hist,
                                                                          'products': products})


@login_required
@user_passes_test(lambda u: u.is_staff)
def product_inventory_view(request, product_id=None):
    products = [p.as_dict() for p in Product.objects.all()]

    if product_id is None:
        return render(request, 'dashboard/inventory/product_inventory.html', {'products': products})

    product = Product.objects.get(pk=int(product_id))
    stocks = product.stock_set.all()
    offices = Office.objects.all()

    if request.method == 'POST':
        quantity = int(request.POST.get('quantity'))
        if request.GET.get('action') == 'add':
            office = Office.objects.get(pk=int(request.POST.get('office_id')))
            if not Stock.objects.filter(office=office, product=product).count():
                s = Stock.objects.create(office=office, product=product, quantity=quantity)
                StockTransfer.objects.create(from_stock=None, to_stock=s, quantity=quantity)
            else:
                s = Stock.objects.filter(office=office, product=product).first()
                s.quantity += quantity
                s.save()
                StockTransfer.objects.create(from_stock=None, to_stock=s, quantity=quantity)

        if request.GET.get('action') == 'move':
            from_office = Office.objects.get(pk=int(request.POST.get('from_office_id')))
            to_office = Office.objects.get(pk=int(request.POST.get('to_office_id')))

            if not Stock.objects.filter(office=to_office, product=product).count():
                Stock.objects.create(office=to_office, product=product)

            to_stock = Stock.objects.filter(office=to_office, product=product).first()
            from_stock = Stock.objects.filter(office=from_office, product=product).first()

            to_stock.quantity += quantity
            from_stock.quantity -= quantity

            to_stock.save()
            from_stock.save()

            StockTransfer.objects.create(from_stock=from_stock, to_stock=to_stock, quantity=quantity)

        return HttpResponseRedirect('')

    return render(request, 'dashboard/inventory/product_inventory.html', {'products': products,
                                                                          'selected_product': product,
                                                                          'stocks': stocks,
                                                                          'offices': offices})