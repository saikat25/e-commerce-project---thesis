from django.shortcuts import render
from django.http.response import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from main.models import *
from main.utils import send_sms
import json
import re


@login_required
@user_passes_test(lambda u: u.is_staff)
def pending_orders_view(request):
    po = Cart.objects.filter(completed_at=None, checkout_at__isnull=False)
    return render(request, 'dashboard/orders/pending_orders.html', {'orders': po})


@login_required
@user_passes_test(lambda u: u.is_staff)
def orders_details_view(request, order_id):
    order = Cart.objects.get(pk=int(order_id))
    return render(request, 'dashboard/orders/order_details.html', {'order': order})


@login_required
@user_passes_test(lambda u: u.is_staff)
def process_order_view(request, order_id):
    order = Cart.objects.get(pk=int(order_id))
    if request.method == 'POST':
        for field in request.POST:
            if re.match(r'cartItem_\d+_stockChange', field):
                changes = json.loads(request.POST.get(field))

                for key in changes.keys():
                    if changes[key] == 0: continue
                    stock = Stock.objects.get(pk=int(key.replace('stock_', '')))
                    stock.quantity -= changes[key]
                    stock.save()

        order.amount_charged = float(request.POST.get('amount_charged')) \
                               if request.POST.get('amount_charged') is not None else order.total
        order.completed_at = now()
        order.save()
        messages.success(request, 'Order has been processed.')

        text = "We've shipped your order (ID: {}) to your delivery address:\n\n{}"
        send_sms(order.contact_phone, text.format(order.id, order.delivery_address))

        return HttpResponse('<script>window.close();</script>')

    cart_items = order.cartitem_set.all()
    return render(request, 'dashboard/orders/process_order.html', {'order': order,
                                                            'cart_items': cart_items})
