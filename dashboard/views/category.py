from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from main.models import *


@login_required
@user_passes_test(lambda u: u.is_staff)
def categories_view(request):
    cats = Category.objects.all()
    return render(request, 'dashboard/category/categories.html', {'categories': cats})


@login_required
@user_passes_test(lambda u: u.is_staff)
def category_create_view(request):
    if request.method == 'POST':
        name = request.POST.get('category_name')
        if name is not None:
            Category.objects.create(name=name)
        messages.success(request, 'Successfully created new category.')
    return HttpResponseRedirect(reverse('categories'))


@login_required
@user_passes_test(lambda u: u.is_staff)
def category_rename_view(request):
    cat_id = request.GET.get('category_id')
    if request.method == 'POST':
        new_name = request.POST.get('category_name')
        if all([cat_id, new_name]):
            cat = Category.objects.get(pk=int(cat_id))
            cat.name = new_name
            cat.save()
        messages.success(request, 'Successfully renamed category.')
    return HttpResponseRedirect(reverse('categories'))


@login_required
@user_passes_test(lambda u: u.is_staff)
def category_delete_view(request):
    cat_id = request.GET.get('category_id')
    if cat_id is not None:
        cat  = Category.objects.get(pk=int(cat_id))
        Product.objects.filter(category=cat).update(category=None)
        cat.delete()
    messages.success(request, 'Successfully deleted category.')
    return HttpResponseRedirect(reverse('categories'))
