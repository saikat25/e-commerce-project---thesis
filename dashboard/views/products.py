from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from main.models import *


@login_required
@user_passes_test(lambda u: u.is_staff)
def products_view(request):
    categories = Category.objects.all()
    category_id = int(request.GET.get('category_id')) if request.GET.get('category_id') is not None else None
    products = Product.objects.filter(category__id=category_id)
    return render(request, 'dashboard/products/products.html', {'category_id': category_id,
                                                                'categories': categories})
