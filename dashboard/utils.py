from django.conf import settings
from main.models import *
import pytz
from datetime import datetime, date, time, timedelta
from django.utils import timezone
from math import ceil
from tempfile import mkstemp
import xlwt


BST = pytz.timezone(settings.TIME_ZONE)
TODAY_MIN = datetime.combine(date.today(), time.min).replace(tzinfo=BST)
TODAY_MAX = datetime.combine(date.today(), time.max).replace(tzinfo=BST)


def tz_aware_datetime_from_string(string):
    return datetime.strptime(string, '%Y-%m-%d %H:%M:%S').replace(tzinfo=BST)


def get_all_sales_in_range(start, end, is_complete=True):
    return Cart.objects.filter(checkout_at__isnull=(not is_complete), completed_at__range=(start, end))


def item_sale_count(item_instance, **kwargs):
    start = kwargs.get('start', TODAY_MIN)
    end = kwargs.get('end', TODAY_MAX)
    items = item_instance.cartitem_set.filter(cart__completed_at__range=(start, end))
    count = 0
    for item in items:
        count += item.quantity
    return count


def get_hottest_items(start, end, slice_count=5):
    carts = get_all_sales_in_range(start, end)
    item_ids = [
        ([cart_item.product.id, ] * cart_item.quantity)
        for cart_item in CartItem.objects.filter(cart__in=[cart.id for cart in carts])
    ]
    counts = {}
    for item in sum(item_ids, []):
        item = Product.objects.get(pk=item)
        counts[item] = counts[item] + 1 if item in counts else 1
    populars = sorted(counts, key=counts.get, reverse=True)
    return [
        {'y': item_sale_count(item, start=start, end=end),
         'indexLabel': item.name,
         'legendText': item.name}
        for item in populars[:slice_count]
    ]


def weekly_sale_counts():
    data = {}
    today = timezone.now()
    for i in range(0, 7):
        end = today - timezone.timedelta(days=i)
        start = end - timezone.timedelta(hours=23, minutes=59, seconds=59)
        items = get_all_sales_in_range(start, end)
        data[str(start)] = len(items)

    return data


def get_all_items_by_number_of_sales(start, end):
    sales = get_all_sales_in_range(start, end)
    item_ids = [
        ([cart_item.product.id, ] * cart_item.quantity)
        for cart_item in CartItem.objects.filter(cart__in=[sale.id for sale in sales])
    ]
    counts = {}
    for item in sum(item_ids, []):
        item = Product.objects.get(pk=item)
        counts[item] = counts[item] + 1 if item in counts else 1
    populars = sorted(counts, key=counts.get, reverse=True)
    return [
        {'count': item_sale_count(item, start=start, end=end),
         'name': item.name,
         'id': item.id}
        for item in populars
    ]


def generate_xls_1(orders):
    xls = xlwt.Workbook(encoding='utf-8')

    sheet = xls.add_sheet('Sales Report')
    LABELS = ('ID', 'Customer name', 'Order description', 'Charged amount', 'Date & time',)
    for n, label in enumerate(LABELS):
        sheet.write(0, n, label)

    for x, order in enumerate(orders):
        sheet.write(x+1, 0, order.id)
        sheet.write(x+1, 1, order.customer.user.get_full_name())
        details = ""
        for cartitem in order.cartitem_set.all():
            details += "{}\n".format(str(cartitem.product.name))
            details += 'Quantity: {}\n'.format(cartitem.quantity)
            if cartitem.product_variation is not None:
                details += 'Variation: {}\n'.format(cartitem.product_variation.name)
            for att in cartitem.product_attachment.all():
                details += '+ {}\n'.format(att.name)
            details += '-----\n'
        sheet.write(x+1, 2, details)
        sheet.write(x+1, 3, order.amount_charged)
        sheet.write(x+1, 4, "{:%d-%m-%y %I:%M%p}".format(order.completed_at))

    return xls

