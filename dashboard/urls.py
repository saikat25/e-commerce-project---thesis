from django.conf.urls import url
from dashboard import views as dashboard_views


urlpatterns = [
    url(r'^dashboard/$', dashboard_views.dashboard_home_view, name='dashboard'),

    url(r'^dashboard/report/period-sales/$', dashboard_views.period_sales_view, name='period-sales'),
    url(r'^dashboard/report/item-sales/$', dashboard_views.item_sales_view, name='item-sales'),
    url(r'^dashboard/report/product-history/$', dashboard_views.product_history_view, name='product-history'),
    url(r'^dashboard/report/user-history/$', dashboard_views.user_history_view, name='user-history'),
    url(r'^dashboard/report/xls/', dashboard_views.report_to_xls),

    url(r'^dashboard/pending/$', dashboard_views.pending_orders_view, name='pending_orders'),
    url(r'^dashboard/order-details/(?P<order_id>\d+)/$', dashboard_views.orders_details_view, name='order_details'),
    url(r'^dashboard/process-order/(?P<order_id>\d+)/$', dashboard_views.process_order_view, name='process_order'),

    url(r'^dashboard/warranty-claims/$', dashboard_views.warranty_claims_view, name='warranty_claims'),
    url(r'^dashboard/warranty-details/(?P<warranty_id>\d+)/$', dashboard_views.warranty_details_view, name='warranty_details'),

    url(r'^dashboard/categories/$', dashboard_views.categories_view, name='categories'),
    url(r'^dashboard/categories/new/$', dashboard_views.category_create_view, name='category_create'),
    url(r'^dashboard/categories/rename/$', dashboard_views.category_rename_view, name='category_rename'),
    url(r'^dashboard/categories/delete/$', dashboard_views.category_delete_view, name='category_delete'),

    url(r'^dashboard/products/$', dashboard_views.products_view, name='products'),

    url(r'^dashboard/inventory/(?P<product_id>\d+)?/?$', dashboard_views.product_inventory_view, name='product_inv'),
    url(r'^dashboard/inventory-history/$', dashboard_views.inventory_history_view, name='inv_history'),

]
