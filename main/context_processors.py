from django.conf import settings
from main.models import Cart


def my_cart(request):
    if not request.user.is_authenticated():
        return {}

    live_cart = Cart.objects.filter(customer=request.user.customer, checkout_at=None)

    if live_cart.count() == 0:
        return {}

    live_cart = live_cart.first()

    cart_items = live_cart.cartitem_set.all()

    return {'cart_items': cart_items,
            'total_price': live_cart.total}


def site_info(_):
    return {'SITE_TITLE': settings.SITE_TITLE}