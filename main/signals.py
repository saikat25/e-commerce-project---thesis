from django.db.models.signals import post_save
from django.contrib.auth.models import User
from main.models import Customer


def create_missing_customer(*_, **kwargs):
    instance = kwargs.get('instance')

    try:
        _ = instance.customer.id is None
    except:
        Customer(user=instance).save()


post_save.connect(create_missing_customer, sender=User)
