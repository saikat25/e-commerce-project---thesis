import os
from django.conf import settings
from django.db import models
from django.db.models import Avg
from django.contrib.auth.admin import User
from django.utils.timezone import now, timedelta


class Customer(models.Model):
    user = models.OneToOneField(User, related_name='customer')
    photo = models.ImageField('Photo', null=True, blank=True, upload_to=settings.USER_PHOTOS_DIR)
    home_address = models.TextField('Home address', null=True, blank=True)
    phone_number = models.CharField('Phone number', max_length=15, null=True, blank=True)

    def __str__(self):
        return 'Customer < User: {}'.format(self.user.username)

    def get_photo_url(self):
        if not self.photo:
            return settings.BLANK_PROFILE_PHOTO_URL
        return str(self.photo).replace(settings.MEDIA_ROOT, settings.MEDIA_URL)


class Office(models.Model):
    name = models.CharField('Name', max_length=50)
    office_type = models.CharField('Office type', max_length=10, choices=(('branch', 'Branch office'),
                                                                          ('head', 'Head office'),))
    manager = models.ForeignKey(User, related_name='manager')
    postal_address = models.TextField('Office address', null=True, blank=True)
    phone_number = models.CharField('Phone number', max_length=15, null=True, blank=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField('Name', max_length=25)

    def __str__(self):
        return 'Category: {}'.format(self.name)

    class Meta:
        verbose_name_plural = 'Categories'


class InstallmentScheme(models.Model):
    name = models.CharField('Name', max_length=15)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField('Name', max_length=100)
    description = models.TextField('Description', blank=True, null=True)
    short_description = models.TextField('Short description', blank=True, null=True)
    default_price = models.FloatField('Default price', default=0.0)
    photo = models.ImageField('Photo', null=True, blank=True, upload_to=settings.PHOTO_DIR)
    photo_caption = models.CharField('Photo caption', max_length=30, null=True, blank=True)
    warranty_period = models.IntegerField('Warranty period (month)', default=0)
    offered_schemes = models.ManyToManyField(InstallmentScheme, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    rating = models.FloatField('Product rating', blank=True, null=True)
    is_featured = models.BooleanField('Featured', default=False, help_text='Show this product in front page')

    def __str__(self):
        return 'Product: {}'.format(self.name)

    @property
    def stock_quantity(self):
        quantity = 0
        for stock in self.stock_set.all():
            quantity += stock.quantity
        return quantity

    @property
    def avg_rating(self):
        if self.rating is not None:
            return self.rating
        return Review.objects.filter(product=self).aggregate(Avg('rating')).get('rating__avg') or 0.0

    def get_photo_url(self):
        if not self.photo:
            return settings.BLANK_ITEM_PHOTO_URL
        return settings.MEDIA_URL + str(self.photo).replace(settings.MEDIA_ROOT, '')

    def as_dict(self):
        return {'id': self.id, 'name': self.name}


class ProductVariation(models.Model):
    name = models.CharField('Name', max_length=50)
    custom_price = models.FloatField('Custom price', default=0.0)
    product = models.ForeignKey(Product)

    def __str__(self):
        return 'Variation: {}'.format(self.name)


class ProductAttachment(models.Model):
    name = models.CharField('Name', max_length=50)
    extra_price = models.FloatField('Extra price', default=0.0)
    product = models.ForeignKey(Product)

    def __str__(self):
        return 'Attachment: {}'.format(self.name)


class Stock(models.Model):
    office = models.ForeignKey(Office)
    product = models.ForeignKey(Product)
    quantity = models.IntegerField('Quantity', default=0)

    def __str__(self):
        return 'Stock @ {} ({})'.format(self.office.name, self.quantity)


class Cart(models.Model):
    customer = models.ForeignKey(Customer)
    checkout_at = models.DateTimeField('Sold at', blank=True, null=True)
    delivery_address = models.TextField('Delivery address', null=True, blank=True)
    contact_phone = models.CharField('Contact phone', max_length=15, null=True, blank=True)
    completed_at = models.DateTimeField('Completed at', blank=True, null=True)
    amount_charged = models.FloatField('Amount charged', blank=True, null=True)

    def __str__(self):
        return 'Cart #{}'.format(self.id)

    @property
    def total(self):
        if self.amount_charged is not None:
            return self.amount_charged
        return sum([x.subtotal for x in self.cartitem_set.all()])


class CartItem(models.Model):
    product = models.ForeignKey(Product)
    product_variation = models.ForeignKey(ProductVariation, blank=True, null=True)
    product_attachment = models.ManyToManyField(ProductAttachment, blank=True)
    quantity = models.IntegerField('Quantity', default=1)
    chosen_scheme = models.ForeignKey(InstallmentScheme, blank=True, null=True)
    cart = models.ForeignKey(Cart)

    def __str__(self):
        return 'Sale item #{}'.format(self.id)

    @property
    def subtotal(self):
        base_price = self.product.default_price if self.product_variation is None else self.product_variation.custom_price
        if base_price == 0.0: base_price = self.product.default_price
        extra_price = 0.0
        for att in self.product_attachment.all():
            extra_price += att.extra_price
        return (base_price + extra_price) * self.quantity

    @property
    def warranty_available(self):
        warranty_period = timedelta(days=(self.product.warranty_period * 30))
        time_since_purchase = now() - self.cart.completed_at
        return warranty_period > time_since_purchase


class Review(models.Model):
    customer = models.ForeignKey(Customer)
    product = models.ForeignKey(Product)
    rating = models.IntegerField('Rating number', default=0)
    message = models.TextField('Message', blank=True, null=True)

    def __str__(self):
        return 'Review #{}'.format(self.id)


class WarrantyClaim(models.Model):
    cartitem = models.ForeignKey(CartItem)
    issue_details = models.TextField('Issue details')
    claim_time = models.DateTimeField('Claim date & time', auto_now_add=True)
    warranty_status = models.CharField('Status', max_length=10, default='claimed',
                                       choices=(('claimed', 'Claimed'),
                                                ('accepted', 'Accepted'),
                                                ('rejected', 'Rejected'),
                                                ('resolved', 'Resolved')))

    def __str__(self):
        return 'Warranty #{}'.format(self.id)


class StockTransfer(models.Model):
    from_stock = models.ForeignKey(Stock, related_name='from_stock', blank=True, null=True)
    to_stock = models.ForeignKey(Stock, related_name='to_stock')
    quantity = models.IntegerField('Quantity', default=1)
    transferred_at = models.DateTimeField('Transferred_at', auto_now_add=True)

    def __str__(self):
        return 'Transfer #{}'.format(self.id)


# import for registering signals
import main.signals
