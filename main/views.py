import os
import datetime
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib import messages
from django.db.models.query_utils import Q
from main.utils import handle_photo_upload, send_sms, recalculate_rating_cache
from main.models import *
from main.forms import RegisterForm, PhotoUploadForm, ProfileEditForm


def home_view(request):
    featured = Product.objects.filter(is_featured=True)[:5]
    return render(request, 'main/home.html', {'products': featured})


def product_list_view(request, category_id=None):
    categories = Category.objects.all()
    query = request.GET.get('query')

    if category_id is None and query is None:
        if categories.count() < 1:
            return HttpResponseRedirect('/')
        category_id = categories.first().id
        return HttpResponseRedirect(reverse('product_list', args=[category_id]))

    if query:
        current_category = None
        products = Product.objects.filter(Q(name__icontains=query) | Q(description__icontains=query))
    else:
        current_category = Category.objects.get(pk=category_id)
        products = Product.objects.filter(category_id=category_id)

    order_by = request.GET.get('order', 'name')
    if order_by not in ['name', 'default_price', 'rating']:
        order_by = 'name'

    if request.GET.get('ascending', 'yes') == 'no':
        order_by = '-{}'.format(order_by)

    products = products.order_by(order_by)

    return render(request, 'main/product_list.html', {'categories': categories,
                                                      'current_category': current_category,
                                                      'products': products})


def product_details_view(request, product_id):
    product = Product.objects.get(pk=product_id)
    variations = ProductVariation.objects.filter(product=product)
    attachments = ProductAttachment.objects.filter(product=product)
    return render(request, 'main/product_details.html', {'product': product,
                                                         'variations': variations,
                                                         'attachments': attachments})


@login_required
def add_review_view(request, product_id):
    product = Product.objects.get(pk=product_id)
    rating = int(request.POST.get('rating', 1))
    message = request.POST.get('message', 'N/A')
    review = Review(rating=rating, message=message, product=product, customer=request.user.customer)
    review.save()
    recalculate_rating_cache(product.id)
    return HttpResponseRedirect(reverse('product_details', args=[product.id]))


@login_required
def remove_review_view(request, review_id):
    review = Review.objects.get(pk=int(review_id))
    review.delete()
    messages.success(request, "The review has been deleted.")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def add_to_cart_view(request, product_id):
    if Cart.objects.filter(customer=request.user.customer, checkout_at=None).count() == 0:
        Cart.objects.create(customer=request.user.customer)

    live_cart = Cart.objects.filter(customer=request.user.customer, checkout_at=None).first()
    product = Product.objects.get(pk=product_id)

    if product.stock_quantity < int(request.POST.get('quantity', 1)):
        messages.error(request, "This product doesn't have enough stock for your request!")
        return HttpResponseRedirect(reverse('product_details', args=[product.id]))

    sale_item = CartItem(cart=live_cart,
                         product=product)

    if request.POST.get('variation', None) is not None:
        sale_item.product_variation = ProductVariation.objects.get(pk=int(request.POST.get('variation')))

    chosen_scheme = request.POST.get('chosen_scheme', None)
    if chosen_scheme is not None:
        sale_item.chosen_scheme = None if int(chosen_scheme) == 0 else InstallmentScheme.objects.get(pk=int(chosen_scheme))

    sale_item.quantity = int(request.POST.get('quantity', 1))

    sale_item.save()

    for key in request.POST:
        if key.startswith('attachment_') and request.POST.get(key) == 'on':
            att_id = int(key.replace('attachment_', ''))
            sale_item.product_attachment.add(ProductAttachment.objects.get(pk=att_id))

    sale_item.save()

    messages.success(request, "Product has been successfully added to the cart!")
    return HttpResponseRedirect(reverse('product_details', args=[product.id]))


@login_required
def remove_cartitem_view(request, item_id):
    sale_item = CartItem.objects.get(pk=int(item_id))
    sale_item.delete()
    messages.success(request, "The item has been removed from the cart.")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def cart_clear_view(request):
    live_cart = Cart.objects.filter(customer=request.user.customer, checkout_at=None).first()
    CartItem.objects.filter(cart=live_cart).delete()
    live_cart.delete()
    messages.success(request, "Your cart has been cleared.")
    return HttpResponseRedirect(reverse('home'))


@login_required
def cart_checkout_view(request):
    if request.method == 'POST':
        live_cart = Cart.objects.filter(customer=request.user.customer, checkout_at=None).first()
        live_cart.contact_phone = request.POST.get('phone', request.user.customer.phone_number)
        live_cart.delivery_address = request.POST.get('address', request.user.customer.home_address)
        live_cart.checkout_at = datetime.datetime.now()
        live_cart.save()
        messages.success(request, "Your purchase order has been sent.")

        text = """Dear customer, your order is accepted. Order ID: {}; total charge is BDT {:.2f}. We'll notify you again during shipment."""
        send_sms(live_cart.contact_phone, text.format(live_cart.id, live_cart.total))
        return HttpResponseRedirect(reverse('home'))

    return render(request, 'main/checkout.html')


def register_view(request):
    if request.method == 'POST':
        f = RegisterForm(request.POST)

        if not f.is_valid():
            return render(request, 'registration/register.html', {'errors': f.errors})

        user = User(username=request.POST.get('username'),
                    email=request.POST.get('email'),
                    first_name=request.POST.get('first_name'),
                    last_name=request.POST.get('last_name'),
                    password=make_password(request.POST.get('password')),
                    is_active=True)
        user.save()

        customer = user.customer
        customer.phone_number=request.POST.get('phone_number')
        customer.home_address=request.POST.get('home_address')
        customer.save()

        messages.success(request, "You've registered successfully! Please login.")
        return HttpResponseRedirect(reverse('home'))

    return render(request, 'registration/register.html')


@login_required
def profile_view(request):
    if request.method == 'POST':
        f = ProfileEditForm(request.POST)
        if f.is_valid():
            customer = request.user.customer
            customer.home_address = request.POST.get('address')
            customer.phone_number = request.POST.get('phone')
            customer.save()
            messages.success(request, 'Your profile has been updated.')
        else:
            messages.error(request, f.errors)
        return HttpResponseRedirect(reverse('profile'))

    purchases = Cart.objects.filter(customer=request.user.customer, completed_at__isnull=False)

    return render(request, 'registration/profile.html', {'profile': request.user.customer,
                                                         'purchases': purchases})


@login_required
def update_photo_view(request):
    if request.method == 'POST':
        form = PhotoUploadForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                file_path = handle_photo_upload(request)
                request.user.customer.photo = file_path
                request.user.customer.save()
            except:
                messages.add_message(request, messages.ERROR, 'Oops! Something went wrong. Please retry.')
        else:
            messages.add_message(request, messages.ERROR, form.errors)
    return HttpResponseRedirect(reverse('profile'))


@login_required
def delete_photo_view(request):
    try:
        customer = request.user.customer
        image = customer.photo
        if not image:
            return HttpResponseRedirect(reverse('profile'))
        customer.photo = None
        customer.save()
        os.remove(image.path)
    except:
        messages.add_message(request, messages.ERROR, 'Oops! Something went wrong. Please retry.')
    return HttpResponseRedirect(reverse('profile'))


@login_required
def update_password_view(request):
    if request.method == 'POST':
        old_password = request.POST.get('old')
        new_password = request.POST.get('new')
        confirm_password = request.POST.get('confirm')

        _, __, salt, hashed_password = request.user.password.split('$')

        if make_password(old_password, salt=salt) != request.user.password:
            messages.error(request, 'You have entered invalid old password.')
            return HttpResponseRedirect(reverse('profile'))

        if new_password != confirm_password:
            messages.error(request, 'Confirmation did not match with new password.')
            return HttpResponseRedirect(reverse('profile'))

        if len(new_password) < 4:
            messages.error(request, 'New password is too small.')
            return HttpResponseRedirect(reverse('profile'))

        request.user.set_password(new_password)
        request.user.save()
        messages.success(request, 'Your password has been updated.')

    return HttpResponseRedirect(reverse('profile'))


@login_required
def warranty_claim_view(request):
    if request.method == 'POST':
        try:
            cartitem = CartItem.objects.get(pk=int(request.POST.get('cartitem_id')))

            if WarrantyClaim.objects.filter(cartitem=cartitem).count() > 0:
                messages.error(request, 'You have already filed a warranty request for this item.')
                return HttpResponseRedirect(reverse('profile'))

            details = request.POST.get('issue_details')
            WarrantyClaim.objects.create(cartitem=cartitem, issue_details=details)

            messages.success(request, 'Your warranty claim has been filed. We\'ll follow up soon.')

        except:
            pass

    return HttpResponseRedirect(reverse('profile'))
