from django.contrib import admin
from main.models import *

from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin


# admin.site.register(Cart)
#admin.site.register(CartItem)

# admin.site.register(Category)

class ProductVariationInline(admin.TabularInline):
    model = ProductVariation

class ProductAttachmentInline(admin.TabularInline):
    model = ProductAttachment

# class StockInline(admin.TabularInline):
#     model = Stock

class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'stock_quantity']
    filter_horizontal = ['offered_schemes']
    exclude = ['rating']
    inlines = [ ProductVariationInline, ProductAttachmentInline ]

admin.site.register(Product, ProductAdmin)


# class ReviewAdmin(admin.ModelAdmin):
#     list_display = [ 'product', 'customer', 'rating', 'message']
#
# admin.site.register(Review, ReviewAdmin)


admin.site.register(Office)
admin.site.register(InstallmentScheme)


admin.site.unregister(User)
admin.site.unregister(Group)

class CustomerInline(admin.StackedInline):
    model = Customer
    can_delete = False

class CustomUserAdmin(UserAdmin):
    inlines = [ CustomerInline ]

admin.site.register(User, CustomUserAdmin)
