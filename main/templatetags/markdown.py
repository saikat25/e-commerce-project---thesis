from django import template
import markdown2


register = template.Library()

@register.filter(name='markdown')
def markdown(raw_text):
    return markdown2.markdown(raw_text)

