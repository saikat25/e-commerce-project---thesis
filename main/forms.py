from django import forms
from django.contrib.auth.models import User


class RegisterForm(forms.Form):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(required=True)
    username = forms.CharField(max_length=30, required=True)
    password = forms.CharField(max_length=50, required=True)
    phone_number = forms.CharField(max_length=30, required=False)
    home_address = forms.CharField(max_length=200, required=False)

    def clean_username(self):
        data = self.cleaned_data.get('username', None)
        if data is not None and User.objects.filter(username=data).count() > 0:
            raise forms.ValidationError("This username is already taken!")

    def clean_email(self):
        data = self.cleaned_data.get('email', None)
        if data is not None and User.objects.filter(email=data).count() > 0:
            raise forms.ValidationError("Account with this email already exists!")


class PhotoUploadForm(forms.Form):
    photo = forms.ImageField(required=True)

    def clean_photo(self):
         photo = self.cleaned_data.get('photo', False)
         if photo:
             if photo._size > 5 * 1024 * 1024:
                   raise forms.ValidationError("File too large. Upload a photo smaller than 5MB.")
             return photo


class ProfileEditForm(forms.Form):
    phone = forms.CharField(max_length=30, required=True)
    address = forms.CharField(max_length=200, required=False)
