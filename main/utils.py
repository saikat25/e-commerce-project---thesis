import os
from django.conf import settings
from main.models import Product, Review
from django.db.models import Avg
from twilio.rest import TwilioRestClient


def handle_photo_upload(request):
    photo = request.FILES['photo']
    file_ext = photo.content_type.split('/')[1]
    file_name = 'photo_{}.{}'.format(request.user.id, file_ext)
    file_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, settings.USER_PHOTOS_DIR, file_name)
    with open(file_path, 'wb+') as destination:
        for chunk in photo.chunks():
            destination.write(chunk)
    return file_path


def recalculate_rating_cache(product_id):
    product = Product.objects.get(pk=product_id)
    product.rating = Review.objects.filter(product=product).aggregate(Avg('rating')).get('rating__avg') or 0.0
    product.save()


def send_sms(to, body):
    if not settings.SMS_ACTIVE:
        print("SMS (To:{}) -> {}".format(to, body))
    else:
        try:
            client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
            client.messages.create(body=body, to=to, from_=settings.TWILIO_NUMBER)
        except Exception as e:
            print(e.message)
