from django.conf.urls import url
from django.contrib.auth import views as auth_views
from main.views import *


urlpatterns = [
    url(r'^$', home_view, name='home'),

    url(r'^accounts/register/$', register_view, name='register'),
    url(r'^accounts/login/$', auth_views.login, {'template_name': 'registration/login.html'}, name='login'),
    url(r'^accounts/logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^accounts/profile/$', profile_view, name='profile'),
    url(r'^accounts/profile/update-password/$', update_password_view, name='update_password'),
    url(r'^accounts/profile/update-photo$', update_photo_view, name='update_photo'),
    url(r'^accounts/profile/delete-photo$', delete_photo_view, name='delete_photo'),

    url(r'^category/(?P<category_id>\d+)?/?$', product_list_view, name='product_list'),
    url(r'^search/$', product_list_view, name='search'),
    url(r'^product/(?P<product_id>\d+)/$', product_details_view, name='product_details'),
    url(r'^product/(?P<product_id>\d+)/add_review/$', add_review_view, name='product_add_review'),
    url(r'^product/(?P<product_id>\d+)/add_to_cart/$', add_to_cart_view, name='product_add_to_cart'),
    url(r'^remove-review/(?P<review_id>\d+)/$', remove_review_view, name='remove_review'),

    url(r'^cart/checkout/$', cart_checkout_view, name='cart_checkout'),
    url(r'^cart/remove-item/(?P<item_id>\d+)/$', remove_cartitem_view, name='remove_cartitem'),
    url(r'^cart/clear/$', cart_clear_view, name='cart_clear'),

    url(r'^warranty-claim/$', warranty_claim_view, name='warranty_claim'),
]