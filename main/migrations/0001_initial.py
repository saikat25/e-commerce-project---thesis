# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sold_at', models.DateTimeField(null=True, verbose_name=b'Sold at', blank=True)),
                ('completed_at', models.DateTimeField(null=True, verbose_name=b'Completed at', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CartItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(default=1, verbose_name=b'Quantity')),
                ('cart', models.ForeignKey(to='main.Cart')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=25, verbose_name=b'Name')),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(upload_to=b'photos', null=True, verbose_name=b'Photo', blank=True)),
                ('home_address', models.TextField(null=True, verbose_name=b'Home address', blank=True)),
                ('phone_number', models.CharField(max_length=15, null=True, verbose_name=b'Phone number', blank=True)),
                ('user', models.OneToOneField(related_name='customer', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='InstallmentScheme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=15, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Name')),
                ('office_type', models.CharField(max_length=10, verbose_name=b'Office type', choices=[(b'branch', b'Branch office'), (b'head', b'Head office')])),
                ('postal_address', models.TextField(null=True, verbose_name=b'Office address', blank=True)),
                ('phone_number', models.CharField(max_length=15, null=True, verbose_name=b'Phone number', blank=True)),
                ('manager', models.OneToOneField(related_name='manager', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=b'Name')),
                ('description', models.TextField(null=True, verbose_name=b'Description', blank=True)),
                ('default_price', models.FloatField(default=0.0, verbose_name=b'Default price')),
                ('photo', models.ImageField(upload_to=b'product_photos', verbose_name=b'Photo')),
                ('warranty_period', models.IntegerField(default=0, verbose_name=b'Warranty period (month)')),
                ('category', models.ForeignKey(to='main.Category')),
                ('offered_schemes', models.ManyToManyField(to='main.InstallmentScheme')),
            ],
        ),
        migrations.CreateModel(
            name='ProductAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Name')),
                ('extra_price', models.FloatField(default=0.0, verbose_name=b'Extra price')),
                ('product', models.ForeignKey(to='main.Product')),
            ],
        ),
        migrations.CreateModel(
            name='ProductVariation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Name')),
                ('custom_price', models.FloatField(default=0.0, verbose_name=b'Custom price')),
                ('product', models.ForeignKey(to='main.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField(default=0, verbose_name=b'Rating number')),
                ('message', models.TextField(null=True, verbose_name=b'Message', blank=True)),
                ('customer', models.ForeignKey(to='main.Customer')),
                ('product', models.ForeignKey(to='main.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(default=0, verbose_name=b'Quantity')),
                ('office', models.ForeignKey(to='main.Office')),
                ('product', models.ForeignKey(to='main.Product')),
            ],
        ),
        migrations.CreateModel(
            name='WarrantyClaim',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('issue_details', models.TextField(verbose_name=b'Issue details')),
                ('claim_time', models.DateTimeField(verbose_name=b'Claim date & time')),
                ('warranty_status', models.CharField(max_length=10, verbose_name=b'Status', choices=[(b'claimed', b'Claimed'), (b'accepted', b'Accepted'), (b'resolved', b'Resolved')])),
                ('cartitem', models.ForeignKey(to='main.CartItem')),
            ],
        ),
        migrations.AddField(
            model_name='cartitem',
            name='chosen_scheme',
            field=models.ForeignKey(to='main.InstallmentScheme'),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='product',
            field=models.ForeignKey(to='main.Product'),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='product_attachment',
            field=models.ManyToManyField(to='main.ProductAttachment', blank=True),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='product_variation',
            field=models.ForeignKey(blank=True, to='main.ProductVariation', null=True),
        ),
        migrations.AddField(
            model_name='cart',
            name='customer',
            field=models.ForeignKey(to='main.Customer'),
        ),
    ]
