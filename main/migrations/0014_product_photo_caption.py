# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_cart_amount_charged'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='photo_caption',
            field=models.CharField(max_length=30, null=True, verbose_name=b'Photo caption', blank=True),
        ),
    ]
