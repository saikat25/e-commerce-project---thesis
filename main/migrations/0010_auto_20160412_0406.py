# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20160222_0105'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='rating',
            field=models.FloatField(null=True, verbose_name=b'Product rating', blank=True),
        ),
        migrations.AlterField(
            model_name='warrantyclaim',
            name='warranty_status',
            field=models.CharField(default=b'claimed', max_length=10, verbose_name=b'Status', choices=[(b'claimed', b'Claimed'), (b'resolved', b'Resolved')]),
        ),
    ]
