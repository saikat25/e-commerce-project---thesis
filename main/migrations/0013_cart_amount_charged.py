# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20160413_0334'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='amount_charged',
            field=models.FloatField(null=True, verbose_name=b'Amount charged', blank=True),
        ),
    ]
