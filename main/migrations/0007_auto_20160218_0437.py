# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20160218_0404'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cart',
            old_name='sold_at',
            new_name='checkout_at',
        ),
    ]
