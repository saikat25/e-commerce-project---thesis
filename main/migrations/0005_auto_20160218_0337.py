# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20160217_2225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='office',
            name='manager',
            field=models.ForeignKey(related_name='manager', to=settings.AUTH_USER_MODEL),
        ),
    ]
