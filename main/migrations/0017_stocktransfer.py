# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_product_is_featured'),
    ]

    operations = [
        migrations.CreateModel(
            name='StockTransfer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(default=1, verbose_name=b'Quantity')),
                ('transferred_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Transferred_at')),
                ('from_stock', models.ForeignKey(related_name='from_stock', blank=True, to='main.Stock', null=True)),
                ('to_stock', models.ForeignKey(related_name='to_stock', to='main.Stock')),
            ],
        ),
    ]
