# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20160218_0337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartitem',
            name='chosen_scheme',
            field=models.ForeignKey(blank=True, to='main.InstallmentScheme', null=True),
        ),
    ]
