# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20160217_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='offered_schemes',
            field=models.ManyToManyField(to='main.InstallmentScheme', blank=True),
        ),
    ]
