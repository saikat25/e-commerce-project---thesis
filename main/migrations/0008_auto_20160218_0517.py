# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20160218_0437'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='contact_phone',
            field=models.CharField(max_length=15, null=True, verbose_name=b'Contact phone', blank=True),
        ),
        migrations.AddField(
            model_name='cart',
            name='delivery_address',
            field=models.TextField(null=True, verbose_name=b'Delivery address', blank=True),
        ),
    ]
