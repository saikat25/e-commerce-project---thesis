# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20160218_0517'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warrantyclaim',
            name='claim_time',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Claim date & time'),
        ),
        migrations.AlterField(
            model_name='warrantyclaim',
            name='warranty_status',
            field=models.CharField(default=b'created', max_length=10, verbose_name=b'Status', choices=[(b'claimed', b'Claimed'), (b'accepted', b'Accepted'), (b'resolved', b'Resolved')]),
        ),
    ]
