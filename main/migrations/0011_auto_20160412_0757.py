# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20160412_0406'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warrantyclaim',
            name='warranty_status',
            field=models.CharField(default=b'claimed', max_length=10, verbose_name=b'Status', choices=[(b'claimed', b'Claimed'), (b'accepted', b'Accepted'), (b'rejected', b'Rejected'), (b'resolved', b'Resolved')]),
        ),
    ]
