//Makes rating stars
function makeRatingStars ($selector) {
    var rating = parseFloat($selector.data('rating'));
    for (var i = 1; i <= 5; i++) {
        var $star = $('<span>');
        if (i <= rating) {
            $star.addClass('glyphicon glyphicon-star');
        } else if (i > rating && i < rating + 1) {
            $star.addClass('glyphicon glyphicon-star').css('color', '#6c7e90');
        } else {
            $star.addClass('glyphicon glyphicon-star-empty');
        }
        $selector.append($star);
    }
}


//Calculates subtotal in product description page
function calcualteTotal() {
    var quantity = parseInt($('input[name=quantity]').val());
    var basePrice = parseFloat($('#subtotal').data('defaultprice'));
    var customPrice = parseFloat($('input[name=variation]:checked').data('customprice'));
    if (!isNaN(customPrice) && customPrice !== 0) {
        basePrice = customPrice;
    }
    var extraPrice = 0.0;
    $('.attachment-list input[type=checkbox]:checked').each(function (_, el) {
        extraPrice += parseFloat($(el).data('extraprice'));
    });
    return (basePrice + extraPrice) * quantity;
}


//Creates new window
function createWindow(url) {
    var popup = window.open(url, '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, width=750, height=600');
    var interval = window.setInterval(function () {
        try {
            if (popup == null || popup.closed) {
                window.clearInterval(interval);
                window.location.reload();
            }
        } catch (e) {}
    }, 1000);
}


// Adjusts requirement in order processing page
function RequirementAdjust(root) {
    this.$root = $(root);
    this.requiredQty = parseInt(this.$root.find('.numRqd').text());
    this.selectedQty = 0;

    this.init();
}

RequirementAdjust.prototype.init = function () {
    var _this = this;
    this.$root.find('.qtySelectorInput').change(function() {
        _this.selectedQty = 0;
        _this.$root.find('.qtySelectorInput').each(function (_, el) {
            _this.selectedQty += parseInt($(el).val());
        });
        if (_this.selectedQty == _this.requiredQty) {
            _this.okay();
        } else {
            _this.notOkay();
        }
    });
};

RequirementAdjust.prototype.okay = function () {
    this.$root.find('.cartItemError').val('0');
    this.$root.find('.numRqdLabel').removeClass('label-danger').addClass('label-success');
    this.updateFormField();
};

RequirementAdjust.prototype.notOkay = function () {
    this.$root.find('.cartItemError').val('1');
    this.$root.find('.numRqdLabel').removeClass('label-success').addClass('label-danger');
};


RequirementAdjust.prototype.updateFormField = function () {
    var cartitemId = this.$root.data('cartitemId');
    var data = {};

    this.$root.find('.qtySelectorInput').each(function (_, el) {
        var $el = $(el);
        data['stock_' + $el.data('stockId')] = parseInt($el.val());
    });

    $('input[name=cartItem_' + cartitemId + '_stockChange]').val(JSON.stringify(data));
};