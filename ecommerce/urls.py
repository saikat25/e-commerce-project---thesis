from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from main.urls import urlpatterns as main_urls
from dashboard.urls import urlpatterns as dashboard_urls


MEDIA_DIR = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
STATIC_DIR = static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
admin.site.site_header = '{} Admin Panel'.format(settings.SITE_TITLE)


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include(main_urls)),
    url(r'', include(dashboard_urls)),
] + MEDIA_DIR + STATIC_DIR